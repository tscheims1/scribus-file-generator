#run with: scribus -g -py scribus-image-importer.py
import scribus
import glob
import os

scribus.openDoc("scibus-template.sla")
if scribus.haveDoc:
  top, left, right, bottom = scribus.getPageMargins()
  pageSize = scribus.getPageSize()



  i = 0
  for f in sorted(glob.glob("./filesToImport/*.png")):
    if(i >0):
      scribus.newPage(-1)
    imageFrame = scribus.createImage(left, top, pageSize[0]-right-left, pageSize[1]-bottom-top)
    frameW, frameH = scribus.getSize(imageFrame)

    scribus.loadImage(f,imageFrame)


    scribus.setScaleImageToFrame(True,True,imageFrame)

    saveScaleX, saveScaleY = scribus.getImageScale(imageFrame)
    scribus.setScaleImageToFrame(1, 0, imageFrame)
    fullScaleX, fullScaleY = scribus.getImageScale(imageFrame)
    scribus.setImageScale(saveScaleX, saveScaleY, imageFrame)

    imageW = frameW * (saveScaleX / fullScaleX)
    imageH = frameH * (saveScaleY / fullScaleY)
    imageX = 0.0
    imageY = 0.0
    imageX = (frameW - imageW) / 2.0
    scribus.setImageOffset(imageX, imageY, imageFrame)
    #set image offset if proportion not exacly machting

    i+=1
  scribus.saveDocAs("test1.sla")



  pdf = scribus.PDFfile()
  pdf.file = 'export.pdf'
  pdf.save()

else:
  print("open document!")
  #
